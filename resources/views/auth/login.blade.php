@extends('admin._layouts.default')

@section('top_header')
@endsection

@section('main')
<div id="front-wrap">
    <p class="heading">SF Box</p>
</div>

<div id="login-wrap">
    <form name="frm_login" id="frm_login" method="post">
        <div>
            <label for="login_name">登入名稱</label>
            <input name="username" id="login_name" type="text" required data-message="請輸入登入名稱" value="">
        </div>
        <div>
            <label for="password">密碼</label>
            <input name="password" id="password" type="password" required data-message="請輸入密碼" value="">
        </div>
        <div class="form-btns"><a href="#" class="btn-submit">登入</a></div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
    </form>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul class="list-unstyled">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>
@endsection

@section('footer')
@endsection

@section('page_js')
<script type="text/javascript">
$(function(){
    $("#frm_login").validator();
    $("#frm_login .btn-submit").click(function(e){
        e.preventDefault();
        $("#frm_login").submit();
    });
    $("#frm_login input").keypress(function(e){
        if(e.keyCode==13){
            $("#frm_login").submit();
        }
    });
});
</script>
@endsection
