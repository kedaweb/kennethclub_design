@extends('admin._layouts.default')

@section('col-rht')
<form method="post" id="myform" enctype="multipart/form-data" novalidate="novalidate">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="id" value="{{ $user->id or '' }}">
    <div class="col-rht-container">
        <div class="sec-hd">用戶 &gt; {{ isset($user) ? '修改' : '新增' }}</div>
        <div id="tabs">
            <ul>
                <li><a href="#tabs-0" class="ui-tabs-anchor">帳戶資料</a></li>
                <li><a href="#tabs-1" class="ui-tabs-anchor">更改密碼</a></li>
            </ul>
            <div id="tabs-0">
                <table cellspacing="1" id="detail-tbl">
                    <tbody>
                        <tr>
                            <th>登入帳號: </th>
                            <td>
                                <input type="text" id="login_name" name="username" value="{{ $user->username or '' }}" required="" data-message="請輸入登入帳號。" onkeydown="if(event.keyCode==13)event.keyCode=9" {{ isset($user) ? 'readonly' : '' }}>
                            </td>
                        </tr>
                        <tr>
                            <th>帳戶類別: </th>
                            <td>
                                <select id="user_type" name="role_id">
                                    @foreach ($roles as $item)
                                        <option value="{{ $item->id }}" {{ isset($user) && !empty($user->roles) && $user->roles[0]->role_id==$item->id ? "selected" : "" }} >{{ $item->display_name }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th>用戶狀況: </th>
                            <td>
                                <input type="radio" name="is_ban" value="0" checked="" onkeydown="if(event.keyCode==13)event.keyCode=9" class="chk-box" style="width: auto;" {{ isset($user) && $user->is_ban ? "" : "checked" }}><span class="txt-true">有效</span>
                                <input type="radio" name="is_ban" value="1" onkeydown="if(event.keyCode==13)event.keyCode=9" class="chk-box" style="width: auto;" {{ isset($user) && $user->is_ban ? "checked" : "" }}><span class="txt-false">停用</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="tabs-1">
                <table cellspacing="1" id="detail-tbl">
                    <tbody>
                        <tr>
                            <th>新密碼:</th>
                            <td>
                                <input type="password" id="new_password" name="password" value="">
                            </td>
                        </tr>
                        <tr>
                            <th>確認密碼: </th>
                            <td>
                                <input type="password" id="new_password_confirmation" name="password_confirmation" value="" data-equals="new_password" data-message="確認密碼不正確" style="margin-top:2px">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="btns-form">
            <input type="submit" name="btnUpdate" value="{{ isset($user) ? '更新' : '新增' }} ">
        </div>
        <div class="bck-link">
            <a href="javascript:history.go(-1)" style="color: rgb(255, 255, 255);">返回 用戶 列表</a>
        </div>
    </div>
</form>
@endsection

@section('page_js')
@include('component.sco_errormsg');

<script type="text/javascript">
$(document).ready(function () {
    $("#myform").validator({effect: 'reposition'});
    $.tools.validator.addEffect("reposition", function(errors, event) {
        // add new ones
        $('.error').remove();
        $.each(errors, function(index, error) {
            var err_msg = $('<div class="error"><p>'+error.messages[0]+'</p></div>').appendTo("#col-rht");
            err_msg.css({'position':'absolute','top':$("#"+error.input.context.id).position().top,'left':$("#"+error.input.context.id).position().left+$("#"+error.input.context.id).outerWidth()});
        });
        $("#col-rht").scrollTop($('.error:first').position().top);
    // the effect does nothing when all inputs are valid
    }, function(inputs)  {

    });
    
    $(".btn-add-to-right").click(function(e){
        e.preventDefault();
        var selected_opt = $("#"+$(this).parent().data("name")).val();
        $("#"+$(this).parent().data("name")+"_list option:selected").each(function(idx, obj){
            selected_opt+=$(this).attr("value")+";";
        });
        $("#"+$(this).parent().data("name")).val(selected_opt);
        $("#"+$(this).parent().data("name")+"_list option:selected").appendTo("#"+$(this).parent().data("name")+"_selected");
    });
    $(".btn-add-to-left").click(function(e){
        e.preventDefault();
        var selected_opt = $("#"+$(this).parent().data("name")).val();
        $("#"+$(this).parent().data("name")+"_selected option:selected").each(function(idx, obj){
            selected_opt = selected_opt.replace($(this).attr("value")+";","");
        });
        $("#"+$(this).parent().data("name")).val(selected_opt);
        $("#"+$(this).parent().data("name")+"_selected option:selected").appendTo("#"+$(this).parent().data("name")+"_list");
    });
    $(":checkbox").css("width","auto");
    $(":radio").css("width","auto");
    $('#ckfinder-list a').button();
    $('#poll-ans a').button();
    $(".bck-link a").button().css('color','#fff');
    $(".user-item strong").css('display','inline-block').css('padding','0 10px').width(200);
    $( ".ans-sortable" ).sortable({
        //placeholder: "ui-state-highlight"
    });
    $( "#tabs" ).tabs();
});
</script>
@endsection