@extends('admin._layouts.default')

@section('col-rht')
<form method="get" id="myform" name="myform">
<div class="col-rht-container">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="sec-hd">{{$title}} &gt; 列表</div>
    <div class="item-list">
        <div id="node-menu">
            <div id="search-bar">
                <span>關鍵字: </span><input id="keyword" type="text" name="keyword" value="{{$keyword or ""}}"> 
                <button id="btnSearch" name="search_keyword"  type="submit" value="1">搜尋</button>
                <a href="{{action($controller.'@getIndex')}}" id="clearSearch">重設搜尋</a>
            </div>
            <div id="search-bar" class="clear" style="margin-top: 5px">
                <span>開始日期: <input id="from_date" type="text" name="from_date" value="{{$from_date or $now_date}}" class="datepicker"></span>
                <span>結束日期: <input id="to_date" type="text" name="to_date" value="{{$to_date or $now_date}}" class="datepicker"></span>
                <button name="search_date" type="submit" value="1" id="clearSearch">搜尋</button>
            </div>
        </div>
        <div id="tbl-tool">
            <div class="btns">
            <a onclick="checkAll('myform', 'del');" href="javascript:void(0);" >全選</a>
            <a onclick="checkInverse('myform', 'del');" href="javascript:void(0);" >選擇相反</a>
            </div>
            <div class="paging"></div>
        </div>
        <table width="100%" border="0" cellspacing="1" cellpadding="0" id="tbl-list" class="">
            <thead>
                <tr>
                    <th scope="col" class="start"></th>
                    <th scope="col"><span>日期</span></th>
                    @foreach ($fields as $item)
                    <th scope="col"><span>{{$item['display']}}</span></th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
            @foreach ($result as $report)
                @foreach ($report->record as $key => $item)
                <tr>
                    <td>{{$report->report_date}}</td>
                    <td><input type="checkbox" value="{{$item->invoice_no}}" name="del0"></td>
                    @foreach ($fields as $field => $row)
                    @if(isset($row['enum']))
                    <td>{{ $row['enum'][$item->{$field}] or $item->{$field} }}</td>
                    @else
                    <td>{{ $item->{$field} }}</td>
                    @endif
                    @endforeach
                </tr>
                @endforeach
            @endforeach
            </tbody>
        </table>
        <div id="tbl-tool">
            <div class="btns">
            <a onclick="checkAll('myform', 'del');" href="javascript:void(0);"><span>全選</span></a>
            <a onclick="checkInverse('myform', 'del');" href="javascript:void(0);" ><span>選擇相反</span></a>
            </div>
            <div class="paging"></div>
        </div>
    </div>
</div>
</form>
@endsection

@section('page_js')
<script type="text/javascript">
$(document).ready(function() {
    $('.datepicker').datepicker({
        dateFormat: "yy-mm-dd"
    });

    $('#tbl-list').DataTable({
        "searching": false,
        "columnDefs": [{
            "targets": [0],
            "orderable": false,
            "searchable": false
        }]
    });
} );
</script>
@endsection