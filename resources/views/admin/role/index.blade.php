@extends('admin._layouts.default')

@section('col-rht')
<form method="post" id="myform" name="myform">
<div class="col-rht-container">
    <div class="sec-hd">用戶類別  &gt; 列表</div>
    <div class="item-list">
        <div class="toolbars" id="top-tools">
            <span class="btn-toggle"></span>
            <span class="tool-menu">
                <span><a class="btns" href="{{action($controller.'@getAdd')}}">新增</a></span>
            </span>
        </div>
        <div id="node-menu">
            <div id="search-bar"><span>關鍵字: </span>
                <input id="keyword" type="text" name="keyword" value="" onkeypress="if(event.keyCode==13) goSearch(this.form);" onfocus="this.select()"> 
                <a href="#" id="btnSearch" onclick="this.disabled=true;goSearch(document.forms['myform'])">搜尋</a> 
                <a href="list.php?keyword=&amp;date_from=&amp;date_to=&amp;job_role=" id="clearSearch" >重設搜尋</a>
            </div>
        </div>
        <div id="tbl-tool">
            <div class="btns">
            <a onclick="checkAll('myform', 'del');" href="javascript:void(0);" >全選</a>
            <a onclick="checkInverse('myform', 'del');" href="javascript:void(0);" >選擇相反</a>
            </div>
            <div class="paging"></div>
        </div>
        <table width="100%" border="0" cellspacing="1" cellpadding="0" id="tbl-list">
            <tbody>
                <tr>
                    <th scope="col" width="20" class="start"><span style="width:20px"></span></th>
                    <th scope="col" width="170"><span style="width:170px"></span></th>
                    <th scope="col"><span>類別名稱</span></th>
                    <th scope="col"><span>類別別號</span></th>
                </tr>
                @foreach ($lists as $key => $item)
                <tr>
                    <td>
                        <input type="checkbox" value="{{$item->id}}" name="del0">
                    </td>
                    <td class="btns"> 
                        <a href="{{action($controller.'@getEdit', [$item->id ])}}" title="修改/瀏覽" class="oi-pencil"><span><span class="oi" data-glyph="pencil"></span></span></a>
                        <a href="{{action($controller.'@getDelete', [$item->id ])}}" title="刪除" class="oi-delete"><span><span class="oi" data-glyph="delete"></span></span></a>
                        <a href="{{action($controller.'@getEditPermission', [$item->id ])}}" title="權限" class="oi-pencil">權限</a>
                    </td>
                    <td>{{ $item->display_name }}</td>
                    <td>{{ $item->name }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <div id="tbl-tool">
            <div class="btns">
            <a onclick="checkAll('myform', 'del');" href="javascript:void(0);"><span>全選</span></a>
            <a onclick="checkInverse('myform', 'del');" href="javascript:void(0);" ><span>選擇相反</span></a>
            </div>
            <div class="paging"></div>
        </div>
    </div>
</div>
</form>
@endsection

@section('page_js')

@endsection