<script type="text/javascript">
@if (count($errors) > 0)
    var error_str = [];
    @foreach ($errors->all() as $element)
    error_str.push('{{$element}}');
    @endforeach
    console.log(error_str.join('</br>'));
    $.scojs_message(error_str.join('</br>'), $.scojs_message.TYPE_ERROR);
@endif
</script>