<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EntrustSetupTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
         // Create table for storing user-imformation
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('user_realname')->nullable();
            $table->string('user_phone')->nullable();
            $table->string('user_photo')->nullable();
            $table->timestamps();
        });
        // Create table for storing roles
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating roles to users (Many-to-Many)
        Schema::create('role_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id', 'role_id']);
        });

        // Create table for storing permissions
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('display_name')->nullable();
            $table->string('description')->nullable();
            $table->timestamps();
        });

        // Create table for associating permissions to roles (Many-to-Many)
        Schema::create('permission_role', function (Blueprint $table) {
            $table->integer('permission_id')->unsigned();
            $table->integer('role_id')->unsigned();
            $table->foreign('permission_id')->references('id')->on('permissions')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['permission_id', 'role_id']);
        });

        //Create table for storing tags
        Schema::create('tags',function(Blueprint $table){
            $table->increments('id');
            $table->integer('inputer_id')->unsigned();
            $table->foreign('inputer_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('name')->unique();
            $table->timestamps();
        });


        //Create table for storing category
        Schema::create('category',function(Blueprint $table){
            $table->increments('id');
            $table->integer('inputer_id')->unsigned();
            $table->foreign('inputer_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->string('name')->unique();
            $table->timestamps();
        });

        //Create table for storing image information
        Schema::create('image_information',function(Blueprint $table){
            $table->increments('id');
            $table->string('image_name')->unique();
            $table->string('image_path')->nullable();
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('category')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->tinyInteger('status');
            $table->integer('inputer_id')->unsigned();
            $table->foreign('inputer_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('auditor_id')->nullable();
            $table->timestamps();
        });

         //Create table for storing scheme
        Schema::create('scheme',function(Blueprint $table){
            $table->increments('id');
            $table->string('theme')->unique();
            $table->text('remark')->nullable();
            $table->integer('inputer_id')->unsigned();
            $table->foreign('inputer_id')->references('id')->on('category')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('recipient_id')->unsigned();
            $table->foreign('recipient_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->tinyInteger('status');
            $table->dateTime('complete_at')->nullable();
            $table->dateTime('end_at')->nullable();
            $table->timestamps();
        });

        // Create table for associating images to scheme (Many-to-Many)
        Schema::create('scheme_images',function(Blueprint $table){
            $table->increments('id');
            $table->integer('scheme_id')->unsigned();
            $table->foreign('scheme_id')->references('id')->on('scheme')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->integer('image_id')->nullable();
            $table->string('image_path')->nullable();
            $table->tinyInteger('image_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('permission_role');
        Schema::drop('permissions');
        Schema::drop('role_user');
        Schema::drop('roles');
    }
}
