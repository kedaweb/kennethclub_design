<?php

use Illuminate\Database\Seeder;

class EntrustTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::transaction(function()
        {

            DB::table('users')->truncate();
            DB::table('roles')->truncate();
            DB::table('permissions')->truncate();

            DB::table('users')->insert(
                ['name' => 'admin', 'username' => 'admin','email' => 'admin@admin.com','password' => bcrypt('123456')]
            );


            DB::table('roles')->insert([
                ['name' => 'administrator','display_name' => '管理員','description' => '管理員'],
                ['name' => 'designer','display_name' => '设计师','description' => '设计师'],
                ['name' => 'image_seeker','display_name' => '图片搜寻员','description' => '图片搜寻员'],
                ['name' => 'draftsman','display_name' => '绘图员','description' => '绘图员']
            ]);

            $permissions = [
               ['name' => 'users-browse','display_name' => '用户浏览','description' => '用户浏览权限'],
               ['name' => 'users-add','display_name' => '用户新增','description' => '用户新增权限'],
               ['name' => 'users-edit','display_name' => '用户修改','description' => '用户修改权限'],
               ['name' => 'role-browse','display_name' => '角色浏览','description' => '角色浏览权限'],
               ['name' => 'role-add','display_name' => '角色新增','description' => '角色新增权限'],
               ['name' => 'role-edit','display_name' => '角色修改','description' => '角色修改权限'],
               ['name' => 'category-browse','display_name' => '种类浏览','description' => '图片种类浏览权限'],
               ['name' => 'category-add','display_name' => '种类新增','description' => '图片种类新增权限'],
               ['name' => 'category-edit','display_name' => '种类修改','description' => '图片种类修改权限'],
               ['name' => 'category-delete','display_name' => '种类删除','description' => '图片种类删除权限'],
               ['name' => 'tag-browse','display_name' => '标签浏览','description' => '图片标签浏览权限'],
               ['name' => 'tag-add','display_name' => '标签新增','description' => '图片标签新增权限'],
               ['name' => 'tag-edit','display_name' => '标签修改','description' => '图片标签修改权限'],
               ['name' => 'tag-delete','display_name' => '标签删除','description' => '图片标签删除权限'],
               ['name' => 'image-browse','display_name' => '图片浏览','description' => '图片浏览权限'],
               ['name' => 'image-upload','display_name' => '图片上传','description' => '图片上传权限'],
               ['name' => 'image-verify','display_name' => '图片审核','description' => '图片审核权限'],
               ['name' => 'image-delete','display_name' => '图片删除','description' => '图片删除权限'],
               ['name' => 'scheme-browse_all','display_name' => '全部方案浏览','description' => '查看所有方案权限'],
               ['name' => 'scheme-publish','display_name' => '方案发布','description' => '发布方案权限'],
               ['name' => 'scheme-mine','display_name' => '我发布的方案浏览','description' => '查看我发布的方案权限'],
               ['name' => 'task_browse','display_name' => '我的任务浏览','description' => '查看我的任务权限'],
              
            ];
            DB::table('permissions')->insert($permissions);

            DB::table('role_user')->insert(['user_id' => '1','role_id' => '1']);

            for ($i=1; $i <= count($permissions); $i++) { 
                DB::table('permission_role')->insert(['permission_id' => $i,'role_id' => '1']);
            }
        });
    }
}