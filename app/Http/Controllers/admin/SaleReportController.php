<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\BaseController;

use App\User;
use App\Model\Role;

use Auth;
use Validator;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Carbon\Carbon;

class SaleReportController extends BaseController
{

    protected $namespace = 'admin';
    protected $view_prefix = 'admin.sale';
    
    public function __construct(Request $request, Client $client)
    {
        parent::__construct();
        $this->request = $request;
        $this->client = new Client([
            'base_uri' => env('SFBOX_CMS_API2'),
            'timeout'  => 60,
        ]);

        $this->fields = [
            'invoice_no' => ['display' => '訂單號碼'],
            'payment_method' => [
                'display' => '付款方法',
                'enum' => [
                    'aeon/asiapay' => '信用卡',
                    'installment' => '分期',
                    'zeropayment' => '現金',
                ],
            ],
            'region_name_cht' => ['display' => '地區'],
            'payment_gateway_ref_no' => ['display' => '付款平台參考號'],
            'payment_status' => [
                'display' => '付款狀態',
                'enum' => [
                    'success' => '成功',
                    'fail' => '失敗',
                    'waiting' => '未完成',
                ],
            ],
            'order_dt' => ['display' => '下單時間'],
            'payment_dt' => ['display' => '付款時間'],
            'payment_fail_dt' => ['display' => '付款失敗時間'],
            'payment_cancel_dt' => ['display' => '付款取消時間'],
            'customer_id' => ['display' => '客戶帳號'],
        ];
        view()->share('fields', $this->fields);
        view()->share('title', '銷售報告');
    }

    public function getIndex()
    {
        $now = Carbon::now();
        $now_date = $now->toDateString();
        $from_date = $this->request->has('from_date') ? $this->request->input('from_date') : $now->subDay()->toDateString();
        $to_date = $this->request->has('to_date') ? $this->request->input('to_date') : $now->toDateString();
        $parameters['query'] = [
            'from_date' => $from_date,
            'to_date' => $to_date,
        ];

        if($this->request->has('search_keyword')){
            $keyword = $this->request->input('keyword');
            $from_date = null;
            $to_date = null;
            $now_date = null;
            $parameters['query'] = [
                'keyword' => $keyword,
            ];
        }else{
            $keyword = null;
        }

        $response = $this->client->request('GET', 'getSaleReportV2.php', $parameters);
        $content = $response->getBody()->getContents();
        $result = json_decode($content);
        return view($this->view_prefix.'.index', compact('now_date','from_date', 'to_date', 'result', 'keyword'));
    }

    public function getEdit($id)
    {

    }

    public function getAction($box_id, $box_user_id, $invoice_id, $action)
    {
        $parameters['query'] = [
            'box_id' => $box_id,
            'box_user_id' => $box_user_id,
            'invoice_id' => $invoice_id,
            'action' => $action,
        ];

        $response = $this->client->request('GET', 'doButtonPressedOperationReport.php', $parameters);
        $content = $response->getBody()->getContents();
        return $content;
    }

    public function getDelete($id)
    {
        # code...
    }
}
