<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\BaseController;

use App\User;
use App\Model\Role;

use Auth;
use Validator;
use Illuminate\Http\Request;

class UserController extends BaseController
{
    protected $namespace = 'admin';
    protected $redirect_path;

    public function __construct()
    {
        parent::__construct();
        $this->redirect_path = action($this->controller.'@getIndex');
    }

    public function getIndex()
    {
        $data['users'] = User::all();
        $data['roles'] = Role::all();
        return view('admin.user.index', $data);
    }

    public function getCreate(Request $request)
    {
        $data['roles'] = Role::all();
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.user.edit', $data);
    }
    
    public function postCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:255|unique:users',
            'password' => 'required|confirmed|min:6'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $data = $request->all();
            $user = User::create([
                'username' => $data['username'],
                'is_ban' => $data['is_ban'],
                'password' => bcrypt($data['password']),
            ]);
            
            $user->roles()->attach($data['role_id']);

            return redirect($this->redirect_path)->with('message', '新增用戶成功!');
        }
    }

    public function getEdit($id)
    {
        $data['user'] = User::find($id);
        $data['roles'] = Role::all();
        $data['redirect_path'] = $this->redirect_path;
        return view('admin.user.edit', $data);
    }

    public function postEdit(Request $request)
    {
        $input_data = $request->all();
        $validator_term['username'] = 'required|max:255|unique:users,username,'.$input_data['id'];

        if($request->has('password')){
            $validator_term['password'] = 'confirmed|min:6';
        }
        
        $validator = Validator::make($request->all(), $validator_term);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            
            $user = User::find($input_data['id']);
            $user->username = $input_data['username'];
            $user->is_ban = $input_data['is_ban'];

            if($request->has('password')){
                $user->password = bcrypt($input_data['password']);
            }

            $user->save();

            $user->roles()->detach();
            $user->roles()->attach($input_data['role_id']);

            return redirect($this->redirect_path)->with('message', '修改用戶成功!');
        } 
    }

    public function getResetPwd($id)
    {
        $data['user'] = User::find($id);
        return view('admin.user.resetpwd', $data);
    }
    
    public function postResetPwd(Request $request)
    {
        $input_data = $request->all();
        $validator = Validator::make($request->all(), [
            'password' => 'required|confirmed|min:6'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            
            $user = User::find($input_data['id']);
            $user->password = bcrypt($input_data['password']);
            $user->save();

            return redirect($this->redirect_path)->with('message', '修改密碼成功!');
        } 
    }

    public function getDelete($id)
    {
        if($id == 1){
            return redirect($this->redirect_path)->with('message', '默认管理员不能删除!');
        }
        $user = User::find($id);
        $user->delete();
        return redirect($this->redirect_path)->with('message', '刪除成功!');
    }

    public function postEditField(Request $request)
    {
        $input_data = $request->all();

        $row = User::find($input_data['id']);
        $row->$input_data['field'] = $input_data['value'];
        $row->save();

        return response()->json(['info' => '修改成功']);
    }
}
