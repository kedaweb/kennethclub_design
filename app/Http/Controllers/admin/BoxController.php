<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\BaseController;

use App\User;
use App\Model\Role;

use Auth;
use Validator;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Carbon\Carbon;

class BoxController extends BaseController
{

    protected $namespace = 'admin';
    
    public function __construct(Request $request, Client $client)
    {
        parent::__construct();
        $this->request = $request;
        $this->client = new Client([
            'base_uri' => env('SFBOX_CMS_API'),
            'timeout'  => 60,
        ]);

        $this->fields = [
            'delivery_date' => ['display' => '要求日期'],
            'purchase_item_name' => ['display' => '動作'],
            'region_name_cht' => ['display' => '地區'],
            'ref_no' => ['display' => '順箱編號'],
            'box_name_cht' => ['display' => '順箱類型'],
            'activity_status' => [
                'display' => '庫存狀態',
                'enum' => [
                    'delivery_in_progress' =>  '訂單處理中',
                    'shipping_to_customer' =>  '上門派箱中',
                    'box_with_customer' =>  '客戶處理中',
                    'shipping_to_warehouse' =>  '上門收箱中',
                    'box_in_warehouse' =>  '倉儲中', 
                    'contract_end' =>  '完約', 
                    'cancel_order' =>  '取消', 
                ],
            ],
            'location' => ['display' => '庫位'],
            'customer_id' => ['display' => '客戶帳號'],
            'display_name' => ['display' => '客戶姓名'],
            'mobile' => ['display' => '客戶電話'],
            'payment_status' => [
                'display' => '順箱狀態',
                'enum' => [
                    'available' =>  '可供使用',
                    'occupied' =>  '使用中',
                    'damaged' =>  '損毀',
                ],
            ],
            'start_dt' => ['display' => '合約開始日期'],
            'end_dt' => ['display' => '合約結束日期'],
            'duration_day' => ['display' => '租期'],
            'serial_num_1' => ['display' => '保安封條碼1'],
            'serial_num_2' => ['display' => '保安封條碼2'],
            'not_allow_renew_dt' => ['display' => '不允許續約'],
            'payment_method' => ['display' => '付款方法'],
            'payment_status' => ['display' => '付款狀態'],
            'lastupdate_dt' => ['display' => '最後更新時間'],
            'complete_dt' => ['display' => '完成日期'],
        ];
        view()->share('fields', $this->fields);
        view()->share('title', '銷售報告');
    }

    public function getIndex()
    {
        $now = Carbon::now();
        $now_date = $now->toDateString();
        $from_date = $this->request->has('from_date') ? $this->request->input('from_date') : $now->subDay()->toDateString();
        $to_date = $this->request->has('to_date') ? $this->request->input('to_date') : $now->toDateString();
        $parameters['query'] = [
            'from_date' => $from_date,
            'to_date' => $to_date,
        ];

        if($this->request->has('search_keyword')){
            $keyword = $this->request->input('keyword');
            $from_date = null;
            $to_date = null;
            $now_date = null;
            $parameters['query'] = [
                'keyword' => $keyword,
            ];
        }else{
            $keyword = null;
        }

        $response = $this->client->request('GET', 'getOperationReport.php', $parameters);
        $content = $response->getBody()->getContents();
        $result = json_decode($content);
        return view('admin.box.index', compact('now_date','from_date', 'to_date', 'result', 'keyword'));
    }

    public function getEdit()
    {

    }
    
    public function postEdit()
    {

    }

    public function getAction($box_id, $box_user_id, $invoice_id, $action)
    {
        $parameters['query'] = [
            'box_id' => $box_id,
            'box_user_id' => $box_user_id,
            'invoice_id' => $invoice_id,
            'action' => $action,
            'admin_id' => 1,
        ];

        $response = $this->client->request('GET', 'doButtonPressedOperationReport.php', $parameters);
        $content =  json_decode($response->getBody()->getContents());
        if($content->result == 0){
            return redirect()->back()->with('message','修改成功');
        }else{
            return redirect()->back()->with('message', $content->result_message);
        }
    }

    public function getDelete($id)
    {
        # code...
    }

    public function getActivityStatus()
    {
        $data['options']['activity.status'] = [
            [
                'value' => 'delivery_in_progress',
                'label' =>  '訂單處理中'
            ],
            [
                'value' => 'shipping_to_customer',
                'label' =>  '上門派箱中'
            ],
            [
                'value' => 'box_with_customer',
                'label' =>  '客戶處理中'
            ],
            [
                'value' => 'shipping_to_warehouse',
                'label' =>  '上門收箱中'
            ],
            [
                'value' => 'box_in_warehouse',
                'label' =>  '倉儲中'
            ], 
            [
                'value' => 'contract_end',
                'label' =>  '完約'
            ], 
            [
                'value' => 'cancel_order',
                'label' =>  '取消'
            ],
        ];
        return $data;
    }
}
