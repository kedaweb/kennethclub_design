<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\BaseController;

use App\User;
use App\Model\Role;

use Auth;
use Validator;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Carbon\Carbon;

class CustomerController extends BaseController
{

    protected $namespace = 'admin';
    protected $view_prefix = 'admin.customer';
    
    public function __construct(Request $request, Client $client)
    {
        parent::__construct();
        $this->request = $request;
        $this->client = new Client([
            'base_uri' => env('SFBOX_CMS_API'),
            'timeout'  => 60,
        ]);

        $this->fields = [
            'customer_id' => ['display' => '客戶帳號'],
            'display_name' => ['display' => '客戶姓名'],
            'join_dt' => ['display' => '開戶日期'],
            'mobile' => ['display' => '客戶電話'],
            'address' => ['display' => '住址'],
            'address_changed_dt' => ['display' => '是否曾經修改地址'],
            'address_changed_dt' => ['display' => '修改地址日期'],
            'birthday_month' => ['display' => '生日月份'],
            'accept_promo' => ['display' => '是否願意收取推廣'],
            'lang' => ['display' => '使用語言'],
            'ref_no' => ['display' => '順箱編號'],
            'box_name_cht' => ['display' => '順箱類型'],
            'activity_status' => [
                'display' => '庫存狀態',
                'enum' => [
                    'delivery_in_progress' =>  '訂單處理中',
                    'shipping_to_customer' =>  '上門派箱中',
                    'box_with_customer' =>  '客戶處理中',
                    'shipping_to_warehouse' =>  '上門收箱中',
                    'box_in_warehouse' =>  '倉儲中', 
                    'contract_end' =>  '完約', 
                    'cancel_order' =>  '取消', 
                ],
            ],
            'location' => ['display' => '庫位'],
            'start_dt' => ['display' => '合約開始日期'],
            'end_dt' => ['display' => '合約結束日期'],
            'duration_day' => ['display' => '租期(日)'],
            'serial_num_1' => ['display' => '保安封條碼1'],
            'serial_num_2' => ['display' => '保安封條碼2'],
        ];
        view()->share('fields', $this->fields);
        view()->share('title', '客戶');
    }

    public function getIndex()
    {
        $parameters = [];
        if($this->request->has('search_keyword')){
            $keyword = $this->request->input('keyword');
            $parameters['query'] = [
                'keyword' => $keyword,
            ];
        }else{
            $keyword = null;
        }

        $response = $this->client->request('GET', 'getCustomerBoxReport.php', $parameters);
        $content = $response->getBody()->getContents();
        $result = json_decode($content);
        return view($this->view_prefix.'.index', compact('result', 'keyword'));
    }

    public function getEdit($id)
    {

    }

    public function getAction($box_id, $box_user_id, $invoice_id, $action)
    {
        $parameters['query'] = [
            'box_id' => $box_id,
            'box_user_id' => $box_user_id,
            'invoice_id' => $invoice_id,
            'action' => $action,
        ];

        $response = $this->client->request('GET', 'doButtonPressedOperationReport.php', $parameters);
        $content = $response->getBody()->getContents();
        return $content;
    }

    public function getDelete($id)
    {
        # code...
    }
}
