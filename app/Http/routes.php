<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Event::listen("illuminate.query", function($query, $bindings){
//     var_dump($query);//sql 预处理 语句
//     var_dump($bindings);// 替换数据
//     echo "</br>";
// });

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::group(['prefix' => 'cms','namespace' => 'Auth'],function() {
    Route::controller('auth', 'AuthController');
});

//后台权限
// Entrust::routeNeedsPermission('cms/users*', "users-admin", redirect('/admin/'));
// Entrust::routeNeedsPermission('cms/roles*', "roles-admin", redirect('/admin/'));

// 后台控制器路由
Route::group(
    ['middleware' => "auth", 'prefix' => 'cms', 'namespace' => 'admin'],
    function() {
        Route::controller('users', 'UserController');
        Route::controller('roles', 'RoleController');
        Route::controller('box', 'BoxController');
        Route::controller('customer', 'CustomerController');
        Route::controller('inventory', 'BoxInventoryController');
        Route::controller('sale-report', 'SaleReportController');
        Route::get('/', ['as' => 'dashboard', 'uses' => 'HomeController@getIndex']);
    }
);